# iOS-Test Grundlagen
Test deckt arbeiten mit UITableView, APIs und Pods ab.

 Ziel ist es eine App zu entwickeln die eine Ansicht mit Filmen oder Serien

* Einbinden und Nutzen von Alamofire für API Requests
* Es wird die themoviedb API genutzt :
`developers.themoviedb.org/3`
* In der Liste soll der Titel und ein paar Zusatzinfos stehen (z.B. Beliebtheit oder Original Sprache)
* Beim auswählen eines Eintrages soll eine Detailansicht angezeigt werden mit Infos zu dem Ausgewählten Eintrag
* Design und Aufbau ist selbst zu entscheiden

### Zusatz
* Zu jedem Element soll ein passendes Bild angezeigt werden (AlamofireImage Nutzung möglich) 


# iOS-Test Fortgeschritten
Test deckt arbeiten mit MapKit, UITableView, APIs und Pods ab.

Ziel ist es eine App zu entwickeln, die Mensen auf einer Karte darstellt und bei Auswahl einer Mensa deren Menü für den aktuellen Tag angezeigt wird.

* Einbinden und Nutzen von Alamofire für API Requests
* Es wird die openmensa API genutzt:
`https://doc.openmensa.org/api/v2/canteens/`
* Es reicht, wenn eine einzige Mensa angezeigt wird bspw. die Mensa “Dresden, Alte Mensa” mit der Id 79. Der Karten-Marker für die Mensa soll auf der Karte als Default Marker (Icon) dargestellt werden.
* Liste aller Namen der Gerichte vom heutigen Tag der ausgewählten Mensa anzeigen. Dafür soll folgende API benutzt werden:
`https://openmensa.org/api/v2/canteens/{id}/days/{day}/meals/`
* Liste der Speisen mit denn Preisen in einem Separaten View als Liste anzeigen 

### Zusatz:
* Alle Mensen in dem angezeigten Map Bereich laden